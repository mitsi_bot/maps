import maps from "../src/Maps";
import axios from "axios";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const routeReponse = {
    data: JSON.parse(`{
    "routes": [
        {
            "id": "c2c8045b-b1c6-4ba0-9ea3-bb2f5e720ba5",
            "sections": [
                {
                    "id": "73256e0a-d551-4a8a-b769-4a7bc145045b",
                    "type": "vehicle",
                    "departure": {
                        "place": {
                            "type": "place",
                            "location": {
                                "lat": 52.5309837,
                                "lng": 13.384567,
                                "elv": 0.0
                            },
                            "originalLocation": {
                                "lat": 52.5307999,
                                "lng": 13.3847
                            }
                        }
                    },
                    "arrival": {
                        "place": {
                            "type": "place",
                            "location": {
                                "lat": 52.5263761,
                                "lng": 13.3686186,
                                "elv": 0.0
                            },
                            "originalLocation": {
                                "lat": 52.5263999,
                                "lng": 13.3686
                            }
                        }
                    },
                    "summary": {
                        "duration": 153,
                        "length": 1206,
                        "baseDuration": 136
                    },
                    "transport": {
                        "mode": "car"
                    }
                }
            ]
        }
    ]
}`)
};

const geoArrivalResponse = {
    data: JSON.parse(`
{
    "items": [
        {
            "title": "2 Rue d'Echaudieras, 87350 Panazol, France",
            "id": "here:af:streetsection:AjkqSa5qP1NZBXhJDLhaxD:CggIBCDEtbGXARABGgEyKGQ",
            "resultType": "houseNumber",
            "houseNumberType": "PA",
            "address": {
                "label": "2 Rue d'Echaudieras, 87350 Panazol, France",
                "countryCode": "FRA",
                "countryName": "France",
                "state": "Nouvelle-Aquitaine",
                "county": "Haute-Vienne",
                "city": "Panazol",
                "street": "Rue d'Echaudieras",
                "postalCode": "87350",
                "houseNumber": "2"
            },
            "position": {
                "lat": 45.84727,
                "lng": 1.31756
            },
            "access": [
                {
                    "lat": 45.84727,
                    "lng": 1.31748
                }
            ],
            "mapView": {
                "west": 1.31627,
                "south": 45.84637,
                "east": 1.31885,
                "north": 45.84817
            },
            "scoring": {
                "queryScore": 1.0,
                "fieldScore": {
                    "city": 1.0,
                    "streets": [
                        1.0
                    ],
                    "houseNumber": 1.0
                }
            }
        }
    ]
}
`)
};

const geoDepartureResponse = {
    data: JSON.parse(`
{
    "items": [
        {
            "title": "48 Avenue de Labarde, 33300 Bordeaux, France",
            "id": "here:af:streetsection:LyIdHE8wKahltLDH-wYToB:CggIBCDBkP3GARABGgI0OChk",
            "resultType": "houseNumber",
            "houseNumberType": "PA",
            "address": {
                "label": "48 Avenue de Labarde, 33300 Bordeaux, France",
                "countryCode": "FRA",
                "countryName": "France",
                "state": "Nouvelle-Aquitaine",
                "county": "Gironde",
                "city": "Bordeaux",
                "district": "Bordeaux Maritime",
                "street": "Avenue de Labarde",
                "postalCode": "33300",
                "houseNumber": "48"
            },
            "position": {
                "lat": 44.87763,
                "lng": -0.55286
            },
            "access": [
                {
                    "lat": 44.87762,
                    "lng": -0.55278
                }
            ],
            "mapView": {
                "west": -0.55413,
                "south": 44.87673,
                "east": -0.55159,
                "north": 44.87853
            },
            "scoring": {
                "queryScore": 0.96,
                "fieldScore": {
                    "city": 1.0,
                    "streets": [
                        0.99
                    ],
                    "houseNumber": 1.0
                }
            }
        }
    ]
}`)
};

const serviceInput = {
    parameters: {
        fields: {
            any: {
                listValue: {
                    values: [
                        { stringValue: "48 avenue de labarde" },
                        { stringValue: "La Réole" }
                    ]
                }
            }
        }
    }
};

describe("test weather", () => {
    it("should make 3 axios calls and responde", done => {
        mockedAxios.get.mockImplementation(req => {
            if (
                req ==
                "https://geocode.search.hereapi.com/v1/geocode?q=48%20avenue%20de%20labarde&apiKey=myApiKey"
            ) {
                return Promise.resolve(geoDepartureResponse);
            }
            if (
                req ==
                "https://geocode.search.hereapi.com/v1/geocode?q=La%20R%C3%A9ole&apiKey=myApiKey"
            ) {
                return Promise.resolve(geoArrivalResponse);
            } else {
                return Promise.resolve(routeReponse);
            }
        });
        maps(serviceInput, "myApiKey").then((resp: ResponseText) => {
            expect(resp.displayText).toEqual("Le temps que tu vas mettre pour aller de \"48 avenue de labarde\" à \"La Réole\" sera de 00h02");
            expect(mockedAxios.get).toHaveBeenCalledTimes(3);
            done();
        });

    });
});
