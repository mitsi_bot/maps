import express, { Response, Request } from "express";
import maps from "./Maps";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import ServiceInput from "mitsi_bot_lib/dist/src/ServiceInput";
import BaseService from "mitsi_bot_lib/dist/src/BaseService";
const config = require("config-yml");

class Server extends BaseService {
    onPost(request: Request, response: Response): void {
        maps(request.body as ServiceInput, config.app.key)
            .then((res: ResponseText) => {
                response.json(res);
            }).catch((err: Error) => {
                console.error(err);
                response.json(new ResponseText("Maps error"));
            });
    }
}

new Server(express()).startServer();