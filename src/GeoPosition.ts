interface IAddress {
    label: string,
    countryCode: string,
    countryName: string,
    state: string,
    county: string,
    city: string,
    street: string,
    postalCode: string,
    houseNumber: string
  }
  
  interface IPosition {
    lat: number,
    lng: number
  }
  
  interface IMapView {
    west: number,
    south: number,
    east: number,
    north: number
  }
  
  interface IScoring {
    queryScore: number,
    fieldScore: {
      city: number,
      streets: [
        number
      ],
      houseNumber: number
    }
  }
  
  interface IGeoPosition {
    title: string,
    id: string,
    resultType: string,
    houseNumberType: string,
    address: IAddress,
    position: IPosition,
    access: IPosition[],
    mapView: IMapView,
    scoring: IScoring
  }
  
  interface IGeoPositionList {
    items: IGeoPosition[]
  }