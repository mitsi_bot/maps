interface IRouteItemList {
    routes: IRoute[]
}

interface IRoute {
    id: string,
    sections: Section[]
}

interface ILocation {
    lat: number,
    lng: number,
    elv: number
}

interface Place {
    type: string,
    location: ILocation,
    originalLocation: IPosition
}

interface Section {
    id: string,
    type: string,
    departure: {
        place: Place
    },
    arrival: {
        place: Place
    },
    summary: ISummary,
    transport: ITransport
}

interface ITransport {
    mode: string
}

interface ISummary {
    duration: number,
    length: number,
    baseDuration: number
}