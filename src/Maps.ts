import { path, reject } from "ramda";
import axios, { AxiosResponse } from "axios";
import { formatNumber } from "mitsi_bot_lib/dist/src/Helper";
import ServiceInput from "mitsi_bot_lib/dist/src/ServiceInput";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";

export default function maps(result: ServiceInput, apiKey: string): Promise<ResponseText> {
  return new Promise(async (resolve, reject) => {
    const destinationList = path(
      ["parameters", "fields", "any", "listValue", "values"],
      result
    ) as string[];
    const departure = path(["stringValue"], destinationList[0]) as string;
    const arrival = path(["stringValue"], destinationList[1]) as string;

    let departPosition!: IPosition;
    let arrivalPosition!: IPosition;
    [departPosition, arrivalPosition] = await parrallize(departure, arrival, apiKey);
    //find a way
    const hereMapsReponse: AxiosResponse<IRouteItemList> = await axios.get(`https://router.hereapi.com/v8/routes?transportMode=car&origin=${departPosition.lat},${departPosition.lng}&destination=${arrivalPosition.lat},${arrivalPosition.lng}&apiKey=${apiKey}&return=summary`);
    const routesList = path(["data", "routes"], hereMapsReponse) as IRoute[];
    const hours = toHours(routesList[0].sections[0].summary.duration);
    const minutes = toMinutes(routesList[0].sections[0].summary.duration, hours);
    const response = `Le temps que tu vas mettre pour aller de "${departure}" à "${arrival}" sera de ${formatNumber(hours)}h${formatNumber(minutes)}`;
    resolve(new ResponseText(response));
  });

  async function getPosition(url: string): Promise<IPosition> {
    const hereDepartureGeoResponse: AxiosResponse<IGeoPositionList> = await axios.get(url);
    const iGeoPositionListDeparture = path(["data", "items"], hereDepartureGeoResponse) as IGeoPosition[];
    return path(["position"], iGeoPositionListDeparture[0]) as IPosition;
  }

  function toHours(duration: number): number {
    return Math.floor(duration / 60 / 60);
  }

  function toMinutes(duration: number, hours: number): number {
    return Math.floor(duration / 60) - (hours * 60)
  }

  function parrallize(departure: string, arrival: string, apiKey: string): Promise<[IPosition, IPosition]> {
    return Promise.all([
      getPosition(`https://geocode.search.hereapi.com/v1/geocode?q=${encodeURI(departure)}&apiKey=${apiKey}`),
      getPosition(`https://geocode.search.hereapi.com/v1/geocode?q=${encodeURI(arrival)}&apiKey=${apiKey}`)
    ])
  }
}
module.exports = maps;
